//
//  HourglassAppDelegate.m
//  Hourglass
//
//  Created by patrick on 2010/9/22.
//  Copyright Patrick Tsai 2010. All rights reserved.
//

#import "cocos2d.h"

#import "HourglassAppDelegate.h"
#import "GameConfig.h"
#import "RootViewController.h"
#import "GemScene.h"

@implementation HourglassAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(UIApplication*)application {
    // Try to use CADisplayLink director
    // if it fails (SDK < 3.1) use the default director
    if (![CCDirector setDirectorType:kCCDirectorTypeDisplayLink]) {
        [CCDirector setDirectorType:kCCDirectorTypeDefault];
    }
        
    // Default texture format for PNG/BMP/TIFF/JPEG/GIF images
    // It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
    // You can change anytime.
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
    // Init the window and view controller
    window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;

    // make the View Controller a child of the main window
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
    
    // Run the game scene
    [[CCDirector sharedDirector] runWithScene:[GemScene scene]];
}

- (void)applicationWillResignActive:(UIApplication*)application {
    [[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication*)application {
    [[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application {
    [[CCDirector sharedDirector] purgeCachedData];
}

- (void)applicationDidEnterBackground:(UIApplication*)application {
    [[CCDirector sharedDirector] stopAnimation];
}

- (void)applicationWillEnterForeground:(UIApplication*)application {
    [[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication*)application {
    CCDirector* director = [CCDirector sharedDirector];

    [[director openGLView] removeFromSuperview];

    [viewController release];

    [window release];

    [director end];
}

- (void)applicationSignificantTimeChange:(UIApplication*)application {
    [[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
    [[CCDirector sharedDirector] release];
    [window release];
    [super dealloc];
}

@end