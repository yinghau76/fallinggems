//
//  HelloWorldScene.m
//  Hourglass
//
//  Created by patrick on 2010/9/22.
//  Copyright Patrick Tsai 2010. All rights reserved.
//

// Import the interfaces
#import "GemScene.h"
#import "Gem.h"
#import "GemExplosion.h"

#define COLLISION_TYPE_GEM 1

typedef struct GemShape
{
    int numVerts;
    CGPoint* verts;
    CGRect textureCoord;
} GemShape;

static GemShape gGemShapes[kTagGemCount];

static void moveSprite(void* ptr, void* unused) {
    cpShape* shape = (cpShape*)ptr;
    CCSprite* sprite = shape->data;
    if (sprite) {
        cpBody* body = shape->body;

        // TIP: cocos2d and chipmunk uses the same struct to store it's position
        // chipmunk uses: cpVect, and cocos2d uses CGPoint but in reality the are the same
        // since v0.7.1 you can mix them if you want.
        [sprite setPosition:body->p];

        [sprite setRotation:(float)CC_RADIANS_TO_DEGREES(-body->a)];
    }
}

// HelloWorld implementation
@implementation GemScene

+ (id)scene {
    // 'scene' is an autorelease object.
    CCScene* scene = [CCScene node];

    // 'layer' is an autorelease object.
    GemScene* layer = [GemScene node];

    // add layer as a child to scene
    [scene addChild:layer];

    // return the scene
    return scene;
}

- (void)addRandomGem {
    [self addGemX:1024/2 y:700];
}

- (void)addGemX:(float)x y:(float)y {
    int gemTag = random() % kTagGemCount;
    CCSpriteBatchNode* gemNode = (CCSpriteBatchNode*)[self getChildByTag:kTagGems];

    CCSprite* sprite = [CCSprite spriteWithBatchNode:gemNode rect:gGemShapes[gemTag].textureCoord];
    sprite.position = ccp(x, y);
    [gemNode addChild:sprite z:0 tag:gemTag];

    cpBody* body = cpBodyNew(5.0f, cpMomentForPoly(1.0f, gGemShapes[gemTag].numVerts, gGemShapes[gemTag].verts, CGPointZero));

    // set the initial position
    body->p = ccp(x, y);

    // add the body to the space
    cpSpaceAddBody(mSpace, body);

    cpShape* shape = cpPolyShapeNew(body, gGemShapes[gemTag].numVerts, gGemShapes[gemTag].verts, CGPointZero);
    shape->e = 0.0f;        // elasticity
    shape->u = 0.5f;        // friction
    shape->data = sprite;   // associates with the sprite
    shape->collision_type = COLLISION_TYPE_GEM;

    // add the shape to the space
    cpSpaceAddShape(mSpace, shape);
}

static int gemCollisionBegin(cpArbiter* arb, struct cpSpace* space, void* data) {
    return YES;
}

static int gemCollisionPreSolve(cpArbiter* arb, cpSpace* space, void* data) {
    return YES;
}

- (void)addExplosionAtX:(CGFloat)x andY:(CGFloat)y {
    CCParticleSystem* emitter = [[GemExplosion alloc] init];
    emitter.position = cpv(x, y);
    [self addChild:emitter];
}

- (void)addScore:(int)scoreToAdd
{
    mScore += scoreToAdd;
    [mScoreLabel setString:[NSString stringWithFormat:@"%d", mScore]];
}

- (void)removeGems {
    for (NSValue* aShape in mGemsToRemove) {
        cpShape* shape = (cpShape*)[aShape pointerValue];
        CCSprite* gemSprite = (CCSprite*)shape->data;

        [self addExplosionAtX:shape->body->p.x andY:shape->body->p.y];

        cpSpaceRemoveBody(mSpace, shape->body);
        cpSpaceRemoveShape(mSpace, shape);

        [gemSprite removeFromParentAndCleanup:YES];
        
        [self addScore:SCORE_2_GEMS];
    }

    [mGemsToRemove removeAllObjects];
}

- (void)scheduleRemoveGem:(cpShape*)shape {
    [mGemsToRemove addObject:[NSValue valueWithPointer:shape]];
}

static void gemCollisionPostSolve(cpArbiter* arb, cpSpace* space, void* data) {
    GemScene* scene = (GemScene*)data;

    cpShape* shapeA;
    cpShape* shapeB;
    cpArbiterGetShapes(arb, &shapeA, &shapeB);

    CCSprite* gemA = (CCSprite*)shapeA->data;
    CCSprite* gemB = (CCSprite*)shapeB->data;

    if ([gemA tag] == [gemB tag]) {
        //[scene scheduleRemoveGem:shapeA];
        //[scene scheduleRemoveGem:shapeB];
    }
}

static void gemCollisionSeparate(cpArbiter* arb, cpSpace* space, void* data) {
}

static cpVect gWalls[4];

- (void)initSpace {
    int i;
    
    CGSize wins = [[CCDirector sharedDirector] winSize];
    cpBody* staticBody = cpBodyNew(INFINITY, INFINITY);
    mSpace = cpSpaceNew();
    cpSpaceResizeStaticHash(mSpace, 400.0f, 40);
    cpSpaceResizeActiveHash(mSpace, 100, 600);

    mSpace->gravity = ccp(0, 0);
    mSpace->elasticIterations = mSpace->iterations;
    
    i = 0;
    gWalls[i++] = ccp(wins.width - 200.0f, wins.height - 300.0f);
    gWalls[i++] = ccp(wins.width - 200.0f, 100.0f);
    gWalls[i++] = ccp(200.0f, 100.0f);
    gWalls[i++] = ccp(200.0f, wins.height - 300.0f);

    int numVertices = sizeof(gWalls) / sizeof(gWalls[0]);
    for (i = 0; i < numVertices - 1; i++) {
        int start = i;
        int end = (i + 1 >= numVertices) ? 0 : i + 1;
        cpShape* shape = cpSegmentShapeNew(staticBody, gWalls[start], gWalls[end], 0.0f);
        shape->e = 1.0f;
        shape->u = 1.0f;
        cpSpaceAddStaticShape(mSpace, shape);
    }

    cpSpaceAddCollisionHandler(mSpace, COLLISION_TYPE_GEM, COLLISION_TYPE_GEM,
                               gemCollisionBegin,
                               gemCollisionPreSolve,
                               gemCollisionPostSolve,
                               gemCollisionSeparate,
                               self);
}

- (void)addGemBatchNode:(int)tag withFile:(NSString*)file {
    CCSpriteBatchNode* batch = [CCSpriteBatchNode batchNodeWithFile:file capacity:256];
    [self addChild:batch z:0 tag:tag];
}

- (void)initScene {
    GemShape* shape;
    CGPoint* verts;
    
    [self addGemBatchNode:kTagGems withFile:@"gems.png"];

    shape = &gGemShapes[kTagGemAmethyst];
    shape->numVerts = 29;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(117, 2, 68, 73);
    verts[0] = cpv(-6.0f, -34.5f);
    verts[1] = cpv(-11.0f, -32.5f);
    verts[2] = cpv(-14.0f, -30.5f);
    verts[3] = cpv(-20.0f, -24.5f);
    verts[4] = cpv(-24.0f, -17.5f);
    verts[5] = cpv(-27.0f, -7.5f);
    verts[6] = cpv(-27.0f, 6.5f);
    verts[7] = cpv(-25.0f, 14.5f);
    verts[8] = cpv(-23.0f, 19.5f);
    verts[9] = cpv(-19.0f, 25.5f);
    verts[10] = cpv(-15.0f, 29.5f);
    verts[11] = cpv(-12.0f, 31.5f);
    verts[12] = cpv(-10.0f, 32.5f);
    verts[13] = cpv(-3.0f, 34.5f);
    verts[14] = cpv(1.0f, 34.5f);
    verts[15] = cpv(8.0f, 32.5f);
    verts[16] = cpv(10.0f, 31.5f);
    verts[17] = cpv(13.0f, 29.5f);
    verts[18] = cpv(17.0f, 25.5f);
    verts[19] = cpv(21.0f, 19.5f);
    verts[20] = cpv(24.0f, 11.5f);
    verts[21] = cpv(25.0f, 6.5f);
    verts[22] = cpv(25.0f, -7.5f);
    verts[23] = cpv(22.0f, -17.5f);
    verts[24] = cpv(20.0f, -21.5f);
    verts[25] = cpv(18.0f, -24.5f);
    verts[26] = cpv(12.0f, -30.5f);
    verts[27] = cpv(9.0f, -32.5f);
    verts[28] = cpv(4.0f, -34.5f);

    shape = &gGemShapes[kTagGemBeryal];
    shape->numVerts = 16;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(59, 77, 68, 74);
    verts[0] = cpv(-11.0f, -36.0f);
    verts[1] = cpv(-26.0f, -25.0f);
    verts[2] = cpv(-33.0f, -4.0f);
    verts[3] = cpv(-29.0f, 15.0f);
    verts[4] = cpv(-28.0f, 18.0f);
    verts[5] = cpv(-17.0f, 29.0f);
    verts[6] = cpv(-13.0f, 32.0f);
    verts[7] = cpv(5.0f, 33.0f);
    verts[8] = cpv(20.0f, 22.0f);
    verts[9] = cpv(21.0f, 21.0f);
    verts[10] = cpv(28.0f, 0.0f);
    verts[11] = cpv(28.0f, -1.0f);
    verts[12] = cpv(24.0f, -19.0f);
    verts[13] = cpv(23.0f, -21.0f);
    verts[14] = cpv(9.0f, -35.0f);
    verts[15] = cpv(1.0f, -36.0f);

    shape = &gGemShapes[kTagGemBloodstone];
    shape->numVerts = 35;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(269, 77, 53, 78);
    verts[0] = cpv(-4.5f, -28.0f);
    verts[1] = cpv(-10.5f, -26.0f);
    verts[2] = cpv(-13.5f, -24.0f);
    verts[3] = cpv(-19.5f, -18.0f);
    verts[4] = cpv(-21.5f, -15.0f);
    verts[5] = cpv(-23.5f, -11.0f);
    verts[6] = cpv(-25.5f, -5.0f);
    verts[7] = cpv(-26.5f, 0.0f);
    verts[8] = cpv(-26.5f, 10.0f);
    verts[9] = cpv(-25.5f, 15.0f);
    verts[10] = cpv(-23.5f, 21.0f);
    verts[11] = cpv(-21.5f, 25.0f);
    verts[12] = cpv(-19.5f, 28.0f);
    verts[13] = cpv(-13.5f, 34.0f);
    verts[14] = cpv(-10.5f, 36.0f);
    verts[15] = cpv(-8.5f, 37.0f);
    verts[16] = cpv(-5.5f, 38.0f);
    verts[17] = cpv(3.5f, 38.0f);
    verts[18] = cpv(6.5f, 37.0f);
    verts[19] = cpv(10.5f, 35.0f);
    verts[20] = cpv(13.5f, 33.0f);
    verts[21] = cpv(18.5f, 28.0f);
    verts[22] = cpv(20.5f, 25.0f);
    verts[23] = cpv(22.5f, 21.0f);
    verts[24] = cpv(24.5f, 15.0f);
    verts[25] = cpv(25.5f, 10.0f);
    verts[26] = cpv(25.5f, 0.0f);
    verts[27] = cpv(24.5f, -5.0f);
    verts[28] = cpv(22.5f, -11.0f);
    verts[29] = cpv(20.5f, -15.0f);
    verts[30] = cpv(18.5f, -18.0f);
    verts[31] = cpv(13.5f, -23.0f);
    verts[32] = cpv(10.5f, -25.0f);
    verts[33] = cpv(6.5f, -27.0f);
    verts[34] = cpv(2.5f, -28.0f);

    shape = &gGemShapes[kTagGemEmerald];
    shape->numVerts = 18;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(327, 2, 68, 73);
    verts[0] = cpv(-10.0f, -35.5f);
    verts[1] = cpv(-22.0f, -26.5f);
    verts[2] = cpv(-25.0f, -23.5f);
    verts[3] = cpv(-31.0f, -6.5f);
    verts[4] = cpv(-31.0f, 0.5f);
    verts[5] = cpv(-28.0f, 14.5f);
    verts[6] = cpv(-27.0f, 17.5f);
    verts[7] = cpv(-13.0f, 31.5f);
    verts[8] = cpv(-8.0f, 32.5f);
    verts[9] = cpv(7.0f, 32.5f);
    verts[10] = cpv(21.0f, 22.5f);
    verts[11] = cpv(22.0f, 21.5f);
    verts[12] = cpv(29.0f, 0.5f);
    verts[13] = cpv(29.0f, -0.5f);
    verts[14] = cpv(25.0f, -19.5f);
    verts[15] = cpv(24.0f, -21.5f);
    verts[16] = cpv(10.0f, -34.5f);
    verts[17] = cpv(4.0f, -35.5f);

    shape = &gGemShapes[kTagGemGarnet];
    shape->numVerts = 15;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(129, 77, 68, 75);
    verts[0] = cpv(-11.0f, -36.5f);
    verts[1] = cpv(-23.0f, -27.5f);
    verts[2] = cpv(-26.0f, -24.5f);
    verts[3] = cpv(-32.0f, -6.5f);
    verts[4] = cpv(-32.0f, -0.5f);
    verts[5] = cpv(-28.0f, 17.5f);
    verts[6] = cpv(-14.0f, 31.5f);
    verts[7] = cpv(-10.0f, 32.5f);
    verts[8] = cpv(6.0f, 32.5f);
    verts[9] = cpv(21.0f, 21.5f);
    verts[10] = cpv(28.0f, 0.5f);
    verts[11] = cpv(28.0f, -2.5f);
    verts[12] = cpv(24.0f, -20.5f);
    verts[13] = cpv(9.0f, -35.5f);
    verts[14] = cpv(2.0f, -36.5f);

    shape = &gGemShapes[kTagGemOnyx];
    shape->numVerts = 31;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(2, 77, 55, 73);
    verts[0] = cpv(-1.5f, -36.5f);
    verts[1] = cpv(-8.5f, -34.5f);
    verts[2] = cpv(-10.5f, -33.5f);
    verts[3] = cpv(-14.5f, -30.5f);
    verts[4] = cpv(-20.5f, -22.5f);
    verts[5] = cpv(-22.5f, -18.5f);
    verts[6] = cpv(-24.5f, -12.5f);
    verts[7] = cpv(-25.5f, -6.5f);
    verts[8] = cpv(-25.5f, 1.5f);
    verts[9] = cpv(-23.5f, 11.5f);
    verts[10] = cpv(-21.5f, 16.5f);
    verts[11] = cpv(-17.5f, 22.5f);
    verts[12] = cpv(-12.5f, 27.5f);
    verts[13] = cpv(-9.5f, 29.5f);
    verts[14] = cpv(-4.5f, 31.5f);
    verts[15] = cpv(5.5f, 31.5f);
    verts[16] = cpv(8.5f, 30.5f);
    verts[17] = cpv(10.5f, 29.5f);
    verts[18] = cpv(13.5f, 27.5f);
    verts[19] = cpv(19.5f, 21.5f);
    verts[20] = cpv(21.5f, 18.5f);
    verts[21] = cpv(24.5f, 11.5f);
    verts[22] = cpv(26.5f, 2.5f);
    verts[23] = cpv(26.5f, -7.5f);
    verts[24] = cpv(24.5f, -16.5f);
    verts[25] = cpv(20.5f, -24.5f);
    verts[26] = cpv(18.5f, -27.5f);
    verts[27] = cpv(14.5f, -31.5f);
    verts[28] = cpv(11.5f, -33.5f);
    verts[29] = cpv(7.5f, -35.5f);
    verts[30] = cpv(3.5f, -36.5f);

    shape = &gGemShapes[kTagGemPeridot];
    shape->numVerts = 18;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(187, 2, 68, 73);
    verts[0] = cpv(-10.0f, -35.5f);
    verts[1] = cpv(-22.0f, -26.5f);
    verts[2] = cpv(-25.0f, -23.5f);
    verts[3] = cpv(-31.0f, -6.5f);
    verts[4] = cpv(-31.0f, 0.5f);
    verts[5] = cpv(-28.0f, 14.5f);
    verts[6] = cpv(-27.0f, 17.5f);
    verts[7] = cpv(-13.0f, 31.5f);
    verts[8] = cpv(-8.0f, 32.5f);
    verts[9] = cpv(7.0f, 32.5f);
    verts[10] = cpv(21.0f, 22.5f);
    verts[11] = cpv(22.0f, 21.5f);
    verts[12] = cpv(29.0f, 0.5f);
    verts[13] = cpv(29.0f, -0.5f);
    verts[14] = cpv(25.0f, -19.5f);
    verts[15] = cpv(24.0f, -21.5f);
    verts[16] = cpv(10.0f, -34.5f);
    verts[17] = cpv(4.0f, -35.5f);

    shape = &gGemShapes[kTagGemRuby];
    shape->numVerts = 18;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(257, 2, 68, 73);
    verts[0] = cpv(-10.0f, -35.5f);
    verts[1] = cpv(-22.0f, -26.5f);
    verts[2] = cpv(-25.0f, -23.5f);
    verts[3] = cpv(-31.0f, -6.5f);
    verts[4] = cpv(-31.0f, 0.5f);
    verts[5] = cpv(-28.0f, 14.5f);
    verts[6] = cpv(-27.0f, 17.5f);
    verts[7] = cpv(-13.0f, 31.5f);
    verts[8] = cpv(-8.0f, 32.5f);
    verts[9] = cpv(7.0f, 32.5f);
    verts[10] = cpv(21.0f, 22.5f);
    verts[11] = cpv(22.0f, 21.5f);
    verts[12] = cpv(29.0f, 0.5f);
    verts[13] = cpv(29.0f, -0.5f);
    verts[14] = cpv(25.0f, -19.5f);
    verts[15] = cpv(24.0f, -21.5f);
    verts[16] = cpv(10.0f, -34.5f);
    verts[17] = cpv(4.0f, -35.5f);

    shape = &gGemShapes[kTagGemSapphire];
    shape->numVerts = 18;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(397, 2, 68, 73);
    verts[0] = cpv(-10.0f, -35.5f);
    verts[1] = cpv(-22.0f, -26.5f);
    verts[2] = cpv(-25.0f, -23.5f);
    verts[3] = cpv(-31.0f, -6.5f);
    verts[4] = cpv(-31.0f, 0.5f);
    verts[5] = cpv(-28.0f, 14.5f);
    verts[6] = cpv(-27.0f, 17.5f);
    verts[7] = cpv(-13.0f, 31.5f);
    verts[8] = cpv(-8.0f, 32.5f);
    verts[9] = cpv(7.0f, 32.5f);
    verts[10] = cpv(21.0f, 22.5f);
    verts[11] = cpv(22.0f, 21.5f);
    verts[12] = cpv(29.0f, 0.5f);
    verts[13] = cpv(29.0f, -0.5f);
    verts[14] = cpv(25.0f, -19.5f);
    verts[15] = cpv(24.0f, -21.5f);
    verts[16] = cpv(10.0f, -34.5f);
    verts[17] = cpv(4.0f, -35.5f);

    shape = &gGemShapes[kTagGemTopaz];
    shape->numVerts = 18;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(199, 77, 68, 75);
    verts[0] = cpv(-11.0f, -36.5f);
    verts[1] = cpv(-23.0f, -27.5f);
    verts[2] = cpv(-26.0f, -24.5f);
    verts[3] = cpv(-27.0f, -22.5f);
    verts[4] = cpv(-33.0f, -4.5f);
    verts[5] = cpv(-33.0f, -3.5f);
    verts[6] = cpv(-29.0f, 15.5f);
    verts[7] = cpv(-28.0f, 17.5f);
    verts[8] = cpv(-14.0f, 31.5f);
    verts[9] = cpv(-11.0f, 32.5f);
    verts[10] = cpv(6.0f, 33.5f);
    verts[11] = cpv(22.0f, 21.5f);
    verts[12] = cpv(29.0f, 0.5f);
    verts[13] = cpv(29.0f, -0.5f);
    verts[14] = cpv(25.0f, -19.5f);
    verts[15] = cpv(24.0f, -21.5f);
    verts[16] = cpv(10.0f, -35.5f);
    verts[17] = cpv(3.0f, -36.5f);

    shape = &gGemShapes[kTagGemAgate];
    shape->numVerts = 29;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(57, 2, 58, 71);
    verts[0] = cpv(-6.0f, -34.5f);
    verts[1] = cpv(-11.0f, -32.5f);
    verts[2] = cpv(-14.0f, -30.5f);
    verts[3] = cpv(-20.0f, -24.5f);
    verts[4] = cpv(-24.0f, -17.5f);
    verts[5] = cpv(-27.0f, -7.5f);
    verts[6] = cpv(-27.0f, 6.5f);
    verts[7] = cpv(-25.0f, 14.5f);
    verts[8] = cpv(-23.0f, 19.5f);
    verts[9] = cpv(-19.0f, 25.5f);
    verts[10] = cpv(-15.0f, 29.5f);
    verts[11] = cpv(-12.0f, 31.5f);
    verts[12] = cpv(-10.0f, 32.5f);
    verts[13] = cpv(-3.0f, 34.5f);
    verts[14] = cpv(1.0f, 34.5f);
    verts[15] = cpv(8.0f, 32.5f);
    verts[16] = cpv(10.0f, 31.5f);
    verts[17] = cpv(13.0f, 29.5f);
    verts[18] = cpv(17.0f, 25.5f);
    verts[19] = cpv(21.0f, 19.5f);
    verts[20] = cpv(24.0f, 11.5f);
    verts[21] = cpv(25.0f, 6.5f);
    verts[22] = cpv(25.0f, -7.5f);
    verts[23] = cpv(22.0f, -17.5f);
    verts[24] = cpv(20.0f, -21.5f);
    verts[25] = cpv(18.0f, -24.5f);
    verts[26] = cpv(12.0f, -30.5f);
    verts[27] = cpv(9.0f, -32.5f);
    verts[28] = cpv(4.0f, -34.5f);

    shape = &gGemShapes[kTagGemCarnelian];
    shape->numVerts = 32;
    shape->verts = verts = (CGPoint*)malloc(shape->numVerts * sizeof(CGPoint));
    shape->textureCoord = CGRectMake(2, 2, 53, 69);
    verts[0] = cpv(-3.5f, -34.5f);
    verts[1] = cpv(-7.5f, -33.5f);
    verts[2] = cpv(-11.5f, -31.5f);
    verts[3] = cpv(-14.5f, -29.5f);
    verts[4] = cpv(-18.5f, -25.5f);
    verts[5] = cpv(-22.5f, -19.5f);
    verts[6] = cpv(-24.5f, -14.5f);
    verts[7] = cpv(-26.5f, -5.5f);
    verts[8] = cpv(-26.5f, 4.5f);
    verts[9] = cpv(-24.5f, 13.5f);
    verts[10] = cpv(-21.5f, 20.5f);
    verts[11] = cpv(-19.5f, 23.5f);
    verts[12] = cpv(-13.5f, 29.5f);
    verts[13] = cpv(-10.5f, 31.5f);
    verts[14] = cpv(-8.5f, 32.5f);
    verts[15] = cpv(-5.5f, 33.5f);
    verts[16] = cpv(3.5f, 33.5f);
    verts[17] = cpv(6.5f, 32.5f);
    verts[18] = cpv(10.5f, 30.5f);
    verts[19] = cpv(13.5f, 28.5f);
    verts[20] = cpv(17.5f, 24.5f);
    verts[21] = cpv(19.5f, 21.5f);
    verts[22] = cpv(23.5f, 13.5f);
    verts[23] = cpv(25.5f, 4.5f);
    verts[24] = cpv(25.5f, -5.5f);
    verts[25] = cpv(23.5f, -14.5f);
    verts[26] = cpv(21.5f, -19.5f);
    verts[27] = cpv(17.5f, -25.5f);
    verts[28] = cpv(13.5f, -29.5f);
    verts[29] = cpv(10.5f, -31.5f);
    verts[30] = cpv(6.5f, -33.5f);
    verts[31] = cpv(2.5f, -34.5f);
    
    CGSize wins = [[CCDirector sharedDirector] winSize];
    mScoreLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%d", mScore]
                                         fntFile:@"casual.fnt"];
    mScoreLabel.position = ccp(100, wins.height - 20);
    [self addChild:mScoreLabel];

    [self schedule:@selector(step:)];
}

- (id)init {
    if ((self = [super init])) {
        mGemsToRemove = [[NSMutableSet alloc] initWithCapacity:8];
        self.isTouchEnabled = YES;
        self.isAccelerometerEnabled = YES;

        cpInitChipmunk();
        [self initSpace];
        [self initScene];
    }

    return self;
}

- (void)onEnter {
    [super onEnter];

    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / GAME_FPS)];
}

- (void)step:(ccTime)delta {
    int steps = 1;
    CGFloat dt = delta / (CGFloat)steps;

    for (int i = 0; i < steps; i++) {
        cpSpaceStep(mSpace, dt);
    }
    cpSpaceHashEach(mSpace->activeShapes, &moveSprite, nil);
    cpSpaceHashEach(mSpace->staticShapes, &moveSprite, nil);

    [self removeGems];
    if (mNewGemTimer++ > 10) {
        [self addRandomGem];
        mNewGemTimer = 0;
    }
}

- (void)ccTouchesEnded:(NSSet*)touches withEvent:(UIEvent*)event {
    for (UITouch* touch in touches) {
        CGPoint location = [touch locationInView:[touch view]];

        location = [[CCDirector sharedDirector] convertToGL:location];

        [self addGemX:location.x y:location.y];
    }
}

- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration {
    static float prevX = 0, prevY = 0;

#define kFilterFactor 0.5f

    float accelX = (float)acceleration.x * kFilterFactor + (1 - kFilterFactor) * prevX;
    float accelY = (float)acceleration.y * kFilterFactor + (1 - kFilterFactor) * prevY;

    prevX = accelX;
    prevY = accelY;

    CGPoint v = ccp(-accelY, accelX);

    mSpace->gravity = ccpMult(v, 2000);
}

- (void)draw {
    
    
    [super draw];
    
    glColor4f(0.8, 1.0, 0.76, 1.0);
	glLineWidth(6.0f);
    
    int numWalls = sizeof(gWalls) / sizeof(gWalls[0]);
    for (int i = 0; i < numWalls - 1; i++) {
        ccDrawLine(gWalls[i], gWalls[i + 1]);
        ccDrawLine(ccp(gWalls[i].x / 1024, gWalls[i].y / 768), ccp(gWalls[i + 1].x / 1024, gWalls[i + 1].y / 768));
    }
}

@end