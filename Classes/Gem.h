//
//  GemSprite.h
//  Hourglass
//
//  Created by patrick on 2010/9/25.
//  Copyright 2010 Patrick Tsai. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
    kTagGemAmethyst = 0,
    kTagGemBeryal,
    kTagGemBloodstone,
    kTagGemEmerald,
    kTagGemGarnet,
    kTagGemOnyx,
    kTagGemPeridot,
    kTagGemRuby,
    kTagGemSapphire,
    kTagGemTopaz,
    kTagGemAgate,
    kTagGemCarnelian,
    kTagGemCount,
    kTagGems,
};

@interface Gem : NSObject {
    
}

@end
