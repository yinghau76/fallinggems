//
//  GameConfig.h
//  Hourglass
//
//  Created by patrick on 2010/9/22.
//  Copyright Patrick Tsai 2010. All rights reserved.
//

#ifndef __GAME_CONFIG_H
#define __GAME_CONFIG_H

#define GAME_FPS 60

//
// Supported Autorotations:
//		None,
//		UIViewController,
//		CCDirector
//
#define kGameAutorotationNone 0
#define kGameAutorotationCCDirector 1
#define kGameAutorotationUIViewController 2

//
// Define here the type of autorotation that you want for your game
//
#define GAME_AUTOROTATION kGameAutorotationUIViewController

#define SCORE_2_GEMS 100

#endif // __GAME_CONFIG_H