//
//  HourglassAppDelegate.h
//  Hourglass
//
//  Created by patrick on 2010/9/22.
//  Copyright Patrick Tsai 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface HourglassAppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
