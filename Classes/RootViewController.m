//
//  RootViewController.m
//  Hourglass
//
//  Created by patrick on 2010/9/22.
//  Copyright Patrick Tsai 2010. All rights reserved.
//


#import "cocos2d.h"

#import "RootViewController.h"
#import "GameConfig.h"
#import "GemScene.h"

@implementation RootViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {    
    CCDirector* director = [CCDirector sharedDirector];
        
    //
    // Create the EAGLView manually
    //  1. Create a RGB565 format. Alternative: RGBA8
    //	2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
    //
    //
    
    EAGLView* glView = [EAGLView viewWithFrame:[[UIScreen mainScreen] bounds]
                                   pixelFormat:kEAGLColorFormatRGB565 // kEAGLColorFormatRGBA8
                                   depthFormat:0                      // GL_DEPTH_COMPONENT16_OES
                            preserveBackbuffer:NO];
    
    // attach the openglView to the director
    [director setOpenGLView:glView];
    
    // To enable Hi-Red mode (iPhone4)
    //	[director setContentScaleFactor:2];
    
    //
    // VERY IMPORTANT:
    // If the rotation is going to be controlled by a UIViewController
    // then the device orientation should be "Portrait".
    //
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
    [director setDeviceOrientation:kCCDeviceOrientationPortrait];
#else
    [director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
#endif
    
    [director setAnimationInterval:1.0 / 60];
    [director setDisplayFPS:YES];
    
    // make the OpenGLView a child of the view controller
    [self setView:glView];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //
    // There are 2 ways to support auto-rotation:
    //  - The OpenGL / cocos2d way
    //     - Faster, but doesn't rotate the UIKit objects
    //  - The ViewController way
    //    - A bit slower, but the UiKit objects are placed in the right place
    //

#if GAME_AUTOROTATION == kGameAutorotationNone
    //
    // EAGLView won't be autorotated
    //
    return NO;

#elif GAME_AUTOROTATION == kGameAutorotationCCDirector
    //
    // EAGLView will be rotated by cocos2d
    //
    // Sample: Autorotate only in landscape mode
    //
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        [[CCDirector sharedDirector] setDeviceOrientation:kCCDeviceOrientationLandscapeRight];
    }
    else if (interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [[CCDirector sharedDirector] setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
    }

    return NO;

#elif GAME_AUTOROTATION == kGameAutorotationUIViewController
    //
    // EAGLView will be rotated by the UIViewController
    //
    // Sample: Autorotate only in landscpe mode
    //
    // return YES for the supported orientations
    if (interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        return YES;

    return NO;

#else
#error Unknown value in GAME_AUTOROTATION

#endif // GAME_AUTOROTATION


    // Shold not happen
    return NO;
}

//
// This callback only will be called when GAME_AUTOROTATION == kGameAutorotationUIViewController
//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    //
    // Assuming that the main window has the size of the screen
    // BUG: This won't work if the EAGLView is not fullscreen
    ///
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect rect;

    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        rect = screenRect;

    else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
        rect.size = CGSizeMake(screenRect.size.height, screenRect.size.width);

    CCDirector* director = [CCDirector sharedDirector];
    EAGLView* glView = [director openGLView];
    float contentScaleFactor = [director contentScaleFactor];

    if (contentScaleFactor != 1) {
        rect.size.width *= contentScaleFactor;
        rect.size.height *= contentScaleFactor;
    }
    glView.frame = rect;
}

#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
}

@end