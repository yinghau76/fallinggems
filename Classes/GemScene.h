//
//  HelloWorldScene.m
//  Hourglass
//
//  Created by patrick on 2010/9/22.
//  Copyright Patrick Tsai 2010. All rights reserved.
//

@interface GemScene : CCLayer
{
	cpSpace* mSpace;
    NSMutableSet* mGemsToRemove;    // store the list of gems to remove from scene
    CCLabelBMFont* mScoreLabel;
    int mScore;
    int mNewGemTimer;
}

// returns a Scene that contains the HelloWorld as the only child
+(id) scene;
-(void) step: (ccTime) dt;
-(void) addGemX:(float)x y:(float)y;

@end
