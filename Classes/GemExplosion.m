//
//  GemExplosion.m
//  Hourglass
//
//  Created by patrick on 2010/9/27.
//  Copyright 2010 Patrick Tsai. All rights reserved.
//

#import "GemExplosion.h"

@implementation GemExplosion

- (id)init
{
    if ((self = [super initWithTotalParticles:16]) != nil)
    {
        self.duration = 0.5f;
        self.life = 0.5f;
        self.lifeVar = 0.25f;
    }
    
    return self;
}

@end
